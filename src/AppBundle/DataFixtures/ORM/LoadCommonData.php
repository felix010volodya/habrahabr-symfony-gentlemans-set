<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\Seller;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCommonData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
            ->setPassword('some_password')
            ->setUsername('Аноним');

        $manager->persist($user);

        $seller = new Seller();
        $seller
            ->setCompany("Рога и копыта")
            ->setContacts("Кудыкина Гора");

        $manager->persist($seller);

        $seller_user = new User();
        $seller_user
            ->setPassword('some_password')
            ->setUsername('Барыга')
            ->setSeller($seller);

        $manager->persist($seller_user);

        $category = new Category();
        $category->setName('Главная');

        $manager->persist($category);

        $category2 = new Category();
        $category2->setName('Неглавная');

        $manager->persist($category2);

        $product = new Product();
        $product
            ->setSeller($seller)
            ->setName('Подушка для программиста')
            ->setDescription('Подушка специально для программистов. Мягкая и периодически издает звук успешно собранного билда.');

        $manager->persist($product);

        $product2 = new Product();
        $product2
            ->setSeller($seller)
            ->setName('Часы для программиста')
            ->setDescription('Часы, в которых каждая минута содержит 64 секунды, каждый час - 64 минуты, а в сутках 16 часов');

        $manager->persist($product2);

        $product->addCategory($category);
        $product->addCategory($category2);

        $product2->addCategory($category);
        $product2->addCategory($category2);

        $user->addFriend($seller_user);
        $seller_user->addFriend($user);

        $category2->setParent($category);
        $category->addChild($category2);

        $manager->flush();
    }
}
